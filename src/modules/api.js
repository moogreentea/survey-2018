import axios from 'axios';

function login(account) {
  return axios.post('/api/v1/token', account);
}

function logout(token) {
  return axios({
    method: 'post',
    url: '/api/v1/token/destroy',
    data: token,
    headers: { Authorization: `Bearer ${token}` },
  });
}

function refreshToken(token) {
  return axios({
    method: 'post',
    url: '/api/v1/token/refresh',
    data: token,
    headers: { Authorization: `Bearer ${token}` },
  });
}

function submitSurvey(result, token) {
  return axios({
    method: 'post',
    url: '/api/v1/test-result',
    data: result,
    headers: { Authorization: `Bearer ${token}` },
  });
}

export {
  login,
  logout,
  submitSurvey,
  refreshToken,
};

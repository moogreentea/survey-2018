'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  PROXY_SERVER_URL: '"http://localhost:8080"',
  API_PATH: '"/api/v1"',
})

// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import axios from 'axios';
import App from './App';
import router from './router';
import store from './store';

Vue.config.productionTip = false;

// //Config axios
axios.defaults.baseURL = `${process.env.PROXY_SERVER_URL}`;
axios.defaults.withCredentials = true;

window.onbeforeunload = function(){
  return "Are you sure you want to close the window?";
}

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  axios,
  store,
  components: { App },
  template: '<App/>',
});

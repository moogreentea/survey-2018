/* eslint no-param-reassign: off */
import localForage from 'localforage';
import { refreshToken } from '@/modules/api';

export default class AuthManager {
  constructor() {
    localForage.config({
      name: 'Survey',
    });
  }

  getAuth() {
    return localForage.getItem('auth').then((auth) => {
      if (auth) {
        return auth;
      }
      throw new Error();
    });
  }

  checkAuth() {
    const promise = this.getAuth();
    return promise
      .then(auth => this.refreshAuth(auth.token))
      .then(auth => this.setAuth(auth))
      .catch(() => {
        throw new Error();
      });
  }

  refreshAuth(token) {
    const promise = refreshToken(token);
    return promise
      .then((repsonse) => {
        return repsonse.data.data;
      })
      .catch(() => {
        throw new Error();
      });
  }

  setAuth(auth) {
    return localForage.setItem('auth', auth).then(() => localForage.getItem('auth'));
  }

  destroyAuth() {
    return localForage.setItem('auth', null).then(() => localForage.getItem('auth'));
  }

  getProfile() {
    return localForage.getItem('profile').then(profile => profile);
  }

  destroyProfile() {
    return localForage.setItem('profile', null).then(() => localForage.getItem('profile'));
  }

  setProfile(profile) {
    return localForage.setItem('profile', profile).then(() => localForage.getItem('profile'));
  }

  getQuestions() {
    return localForage.getItem('questions').then(questions => questions);
  }

  destroyQuestions() {
    return localForage.setItem('questions', null).then(() => localForage.getItem('questions'));
  }

  setQuestions(questions) {
    return localForage.setItem('questions', questions).then(() => localForage.getItem('questions'));
  }
}

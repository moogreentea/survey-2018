import Vue from 'vue';
import Router from 'vue-router';
import RootChrome from '@/components/layouts/RootChrome';
import Profile from '@/components/pages/Profile';
import QuizeBrowser from '@/components/pages/QuizeBrowser';
import QuizeDetail from '@/components/pages/QuizeDetail';
import QuizeSurvey from '@/components/pages/QuizeSurvey';
import Login from '@/components/pages/Login';
import PrintResult from '@/components/pages/PrintResult';
import ApplicationChrome from '@/components/pages/ApplicationChrome';
import showAllResult from '@/components/pages/showAllResult';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/show/:typeNumber',
      component: showAllResult,
    },
    {
      path: '/login',
      component: Login,
    },
    {
      path: '/',
      name: 'RootChrome',
      component: RootChrome,
      children: [
        {
          path: 'profile',
          component: Profile,
        },
        {
          path: '/',
          component: ApplicationChrome,
          redirect: 'quizzes',
          children: [
            {
              path: 'quizzes',
              component: QuizeBrowser,
              children: [
                {
                  path: '/',
                  component: QuizeDetail,
                },
                {
                  path: 'type/:typeNumber',
                  component: QuizeSurvey,
                },
                {
                  path: ':questionNumber/printed',
                  component: PrintResult,
                },
              ],
            },
          ],
        },
      ],
    },
  ],
  scrollBehavior() {
    return { x: 0, y: 0 };
  },
});

import _ from 'lodash';
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    currentQuestionPrinted: null,
    isProfile: false,
    isLogout: false,
  },
  mutations: {
    setCurrentQuestionPrinted(state, newQuestion) {
      state.currentQuestionPrinted = _.cloneDeep(newQuestion);
    },
    setIsProfile(state) {
      state.isProfile = true;
    },
    setIsLogout(state, value) {
      state.isLogout = value;
    },
  },
  actions: {
    setCurrentQuestionPrinted({ commit }, question) {
      commit('setCurrentQuestionPrinted', question);
    },
    setIsProfile({ commit }) {
      commit('setIsProfile');
    },
    setIsLogout({ commit }, value) {
      commit('setIsLogout', value);
    },
  },
});

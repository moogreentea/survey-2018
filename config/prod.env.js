'use strict'
module.exports = {
  NODE_ENV: '"production"',
  PROXY_SERVER_URL: '"http://api.healthatwork-cu.com"',
  API_PATH: '"/api/v1"',
}
